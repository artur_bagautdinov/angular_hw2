import { Component, EventEmitter, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AddBoardService } from '../services/add-board.service.ts.service';
import { Board } from '../shared/board.model';
import { DeleteBoard } from '../store/boards/boards.actions';
import { selectBoards } from '../store/boards/boards.selectors';
import { StoreState } from '../store/store.model';

@Component({
  selector: 'app-boards-controller',
  templateUrl: './boards-controller.component.html',
  styleUrls: ['./boards-controller.component.scss']
})
export class BoardsControllerComponent {

  public readonly boards$: Observable<Board[]> = this.store.select(selectBoards)
  constructor(private store: Store<StoreState>, private addBoardService: AddBoardService) { }

  onBoardAdd() {
    // this.addBoardService.showModalForm(true)cd
    this.addBoardService.showModalForm();
    // this.onShowAddBoardForm.emit({displayForm: true})

  }

  public deleteBoard(boardCardId: string): void {
    this.store.dispatch(new DeleteBoard(boardCardId))
  }



}
