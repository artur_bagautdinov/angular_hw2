import { Action } from "@ngrx/store";
import { Board } from "src/app/shared/board.model";

export enum BoardsActonType {
  AddBoard = '[Boards] Add Board',
  DeleteBoard = '[Boards] Delete Board'
}

export interface BoardsAction extends Action {
  type: BoardsActonType,
  board?: Board,
  boardId?: string
}
