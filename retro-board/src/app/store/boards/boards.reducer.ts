import { Board } from "src/app/shared/board.model";
import { BoardsAction, BoardsActonType } from "./boards.model";

export function boardsReducer(state: Board[] = [], action: BoardsAction): Board[] {
  switch (action.type) {

    case BoardsActonType.AddBoard:
      return [...state, action.board];

    case BoardsActonType.DeleteBoard:
      return state.filter((board: Board) => board.id !== action.boardId);

    default:
      return state
  }
}
