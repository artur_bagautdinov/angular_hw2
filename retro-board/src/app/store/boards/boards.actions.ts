import { Board } from "src/app/shared/board.model";
import { BoardsAction, BoardsActonType } from "./boards.model";

export class AddBoard implements BoardsAction {
  public readonly type: BoardsActonType.AddBoard = BoardsActonType.AddBoard;

  constructor(public readonly board: Board) {

  }
}


export class DeleteBoard implements BoardsAction {
  public readonly type: BoardsActonType.DeleteBoard = BoardsActonType.DeleteBoard;

  constructor(public readonly boardId: string) {

  }

}
