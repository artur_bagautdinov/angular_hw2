import { Board } from "../shared/board.model";

export interface StoreState {
  boards: Board[]
}
