import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'retro-board';

  // isModalFormShown:boolean = false;

  // showForm(ev){
  //   console.log('emit')
  // }

  // addBoardButtonCicked(){
  //   console.log('EMIT from parent')
  //   this.isModalFormShown = !this.isModalFormShown;
  // }
}
