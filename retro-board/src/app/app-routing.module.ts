import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BoardComponent } from "./board/board.component";
import { BoardsControllerComponent } from "./boards-controller/boards-controller.component";

const routes : Routes = [
  {path: '', component: BoardsControllerComponent},
  {path: ':id', component: BoardComponent}
]

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})

export class AppRoutingmodule {

}
