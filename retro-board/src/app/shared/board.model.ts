export interface Board {
  title: string,
  date: Date | number,
  cardsCount?: number,
  cards?: [],
  id: string
}
