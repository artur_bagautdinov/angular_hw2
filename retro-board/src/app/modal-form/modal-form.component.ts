import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AddBoardService } from '../services/add-board.service.ts.service';
import { AddBoard } from '../store/boards/boards.actions';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalFormComponent {

  isFormActive: boolean = false;


  public readonly boardForm: FormGroup = new FormGroup({
    userName: new FormControl('')
  })

  constructor(private store: Store, private addBoardService: AddBoardService, private cd: ChangeDetectorRef) {

    this.addBoardService.activeModalForm$.subscribe(
      data => {
        this.isFormActive = true;
        console.log(this.isFormActive)
        // this.cd.detectChanges();
      }

    )

  }

  public closeAddBoardForm() : void {
    this.isFormActive = false;
  }

  public addBoard(): void {
    this.store.dispatch(new AddBoard({
      id: Date.now().toString(),
      date: new Date(),
      cardsCount: 0,
      cards: [],
      title: this.boardForm.value.userName
    }))

    this.boardForm.reset();
    this.closeAddBoardForm();
  }

}
