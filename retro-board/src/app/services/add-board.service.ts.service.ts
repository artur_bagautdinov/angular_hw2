import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddBoardService {

  private activeModalForm = new Subject<any>();
  activeModalForm$ = this.activeModalForm.asObservable();

  showModalForm():void {
     this.activeModalForm.next();
  }

  showCardEditForm(): void {
    this.activeModalForm.next();
  }

}
