import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';

import { Board } from '../shared/board.model';
import { DeleteBoard } from '../store/boards/boards.actions';

@Component({
  selector: 'app-board-preview',
  templateUrl: './board-preview.component.html',
  styleUrls: ['./board-preview.component.scss']
})
export class BoardPreviewComponent {

  @Input() public boardCard: Board;
  // @ViewChild('listItem', { static: false }) public listItemRef: ElementRef;
  @Output() public delete: EventEmitter<string> = new EventEmitter();


  constructor(private store: Store) {

  }

  removeBoardCard(boarCardId: string): void {
    this.delete.emit(boarCardId)
    // console.log(this.listItemRef.nativeElement.id)
    // const listItemId: string = this.listItemRef.nativeElement.id
    // this.store.dispatch(new DeleteBoard({
    //   boardId:  boarCardId,
    // }))
  }

}
