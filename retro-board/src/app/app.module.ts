import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BoardsControllerComponent } from './boards-controller/boards-controller.component';
import { BoardPreviewComponent } from './board-preview/board-preview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalFormComponent } from './modal-form/modal-form.component';
import { StoreModule } from '@ngrx/store';
import { boardsReducer } from './store/boards/boards.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BoardComponent } from './board/board.component';
import { BoardColumnComponent } from './board-column/board-column.component';
import { AppRoutingmodule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    BoardsControllerComponent,
    BoardPreviewComponent,
    ModalFormComponent,
    BoardComponent,
    BoardColumnComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingmodule,
    StoreModule.forRoot({
      boards: boardsReducer
    }),
    StoreDevtoolsModule.instrument()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
